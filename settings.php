<?php

namespace Dorigo\CampaignMonitor\Settings;

if(is_admin()) {
    add_action('admin_menu', __NAMESPACE__.'\\menu');
    add_action('admin_init', __NAMESPACE__.'\\register_settings');
}

function menu() {
    add_options_page('Campaign Monitor Settings','Campaign Monitor','manage_options','dorigo_campaign',__NAMESPACE__.'\\settings_output');
}


function settings() {
    $settings = [
        [
            'default' => '',
            'name' => 'dorigo_campaign_api_key',
            'label' => 'API Key',
        ],

        [
            'name' => 'dorigo_campaign_clients',
            'label' => 'Client',
        ],

        [
            'name' => 'dorigo_campaign_list',
            'label' => 'Default List',
        ],

        [
            'name' => 'dorigo_campaign_success',
            'label' => 'Success Message',
            'default' => 'Thanks for subscribing!',
        ],
    ];

    return apply_filters('Dorigo/Social/Settings', $settings);
}


function register_settings() {
    $settings = settings();

    foreach($settings as $setting) {
        register_setting('dorigo_campaign', $setting['name']);
    }
}


function settings_output() {
    $settings = settings();

    echo '<div class="wrap">';
        echo '<h2>Campaign Monitor Settings</h2>';

        echo '<form action="options.php" method="post">';
            settings_fields('dorigo_campaign');

            echo '<p>You get your API key by logging into your <a href="https://www.campaignmonitor.com" target="_blank">campaignmonitor.com</a> account, going to <strong>Account Settings</strong>, then clicking <strong>API Keys</strong>.</p>';
            echo '<p>Make sure to click the “<em>Show API Key</em>” link to get your API Key, instead of your Client ID.</p>';

            echo '<table class="form-table">';
                echo '<tbody>';

                foreach($settings as $setting) {
                    echo '<tr>';
                        echo '<th scope="row">';
                            echo '<label for="'.$setting['name'].'">'.$setting['label'].'</label>';
                        echo '</th>';

                        echo '<td>';
                            $value = get_option($setting['name']);

                            if($setting['name'] === 'dorigo_campaign_clients') {

                                echo '<select name="'.$setting['name'].'" id="'.$setting['name'].'">';
                                    echo '<option value="" disabled'.(!$value?' selected':'').'>Please Select an Account</option>';

                                    if(get_option('dorigo_campaign_api_key')) {
                                        if(!isset($api)) { $api = new \Dorigo\CampaignMonitor(); }

                                        foreach($api->clients() as $client) {
                                            echo '<option value="'.$client->ClientID.'"'.($client->ClientID === $value ? ' selected' :'').'>&nbsp;&nbsp;'.$client->Name.'</option>';
                                        }
                                    }

                                echo '</select>';

                            } else if($setting['name'] === 'dorigo_campaign_list') {

                                echo '<select name="'.$setting['name'].'" id="'.$setting['name'].'">';
                                    echo '<option value="" disabled'.(!$value?' selected':'').'>Please Select a List</option>';

                                    if(get_option('dorigo_campaign_api_key') && get_option('dorigo_campaign_clients')) {
                                        if(!isset($api)) { $api = new \Dorigo\CampaignMonitor(); }

                                        foreach($api->lists() as $client) {
                                            echo '<option value="'.$client->ListID.'"'.($client->ListID === $value ? ' selected' :'').'>&nbsp;&nbsp;'.$client->Name.'</option>';
                                        }
                                    }

                                echo '</select>';

                            } else {

                                echo '<input type="text" name="'.$setting['name'].'" id="'.$setting['name'].'" value="'.($value ?: (isset($setting['default']) ? $setting['default'] : '')).'" class="regular-text ltr">';

                            }

                        echo '</td>';
                    echo '<tr>';

                }

                echo '</tbody>';
            echo '</table>';


            submit_button();
        echo '</form>';

    echo '</div>';
}