<?php

namespace Dorigo;

class CampaignMonitor {
    private $apiKey;
    private $auth;
    private $clients;
    private $list;
    private $successMessage;
    private static $api;
    public static $ajaxAction = 'campaign-monitor-subscribe';

    private static function Instance() {
        if(self::$api === null) {
            self::$api = new \Dorigo\CampaignMonitor();
        }

        return self::$api;
    }

    public function __construct() {
        if($key = get_option('dorigo_campaign_api_key')) {
            $this->apiKey = $key;
        } else {
            self::error($result);
        }

        $this->auth = ['api_key' => $this->apiKey];
        $this->client = get_option('dorigo_campaign_clients');
        $this->list = get_option('dorigo_campaign_list');
        $this->successMessage = get_option('dorigo_campaign_success') ?: 'Thanks for subscribing!';

        self::$api = $this;
    }

    public function clients() {
        $wrap = new \CS_REST_General($this->auth);
        $result = $wrap->get_clients();

        if(!$result->was_successful()) {
            return [];
        }

        return $result->response;
    }

    public function lists($client = null) {
        $client = $client ?: $this->client;

        $wrap = new \CS_REST_Clients($client, $this->auth);
        $result = $wrap->get_lists();

        if(!$result->was_successful()) {
            return [];
        }

        return $result->response;
    }

    public static function subscribeUrl() {
        return admin_url('admin-ajax.php');
    }

    public static function formFields() {
        $fields = [
            '<input type="hidden" name="action" value="'.self::$ajaxAction.'">',
            '<input type="hidden" name="campaign-monitor-form" value="true">',
        ];

        return implode(PHP_EOL, $fields);
    }

    public static function subscribe() {
        self::Instance();

        $client = isset($_GET['client']) ? $_GET['client'] : self::$api->client;
        $list   = isset($_GET['list'])   ? $_GET['list']   : self::$api->list;

        $email  = isset($_GET['email'])  ? $_GET['email']  : false;

        /*if(!$email) {
            self::showResult([
                'Status' => 'success',
                'Code' => $result->http_status_code,
                'Message' => self::$successMessage,
            ]);

            die();
        }*/

        $wrap = new \CS_REST_Subscribers($list, self::$api->auth);

        $result = $wrap->add([
            'EmailAddress' => $email,
            'Resubscribe' => true
        ]);

        if(!$result->was_successful()) {
            self::error($result);
        }

        $result->Status = 'success';
        $result->Code = $result;

        self::showResult([
            'Status' => 'success',
            'Code' => $result->http_status_code,
            'Message' => self::$api->successMessage,
        ]);
    }

    private static function error($result) {
        trigger_error("Failed with error: {$result->http_status_code}. ".$result->response->Message);

        $result->response->Status = 'error';

        self::showResult($result->response, $result->http_status_code);
    }

    private static function showResult($object = [], $status = 200) {
        header('Content-Type: application/json');
        http_response_code($status);

        echo json_encode($object);
        die();
    }

    public static function gdpr() {
        self::Instance();

        $client = self::$api->client;
        $list   = isset($_GET['list'])   ? $_GET['list']   : self::$api->list;

        $name   = isset($_POST['name'])  ? $_POST['name']  : false;
        $email  = isset($_POST['email']) ? $_POST['email'] : false;

        $addr =   isset($_POST['address']) ? $_POST['address'] : '';
        $phone =  isset($_POST['phone'])   ? $_POST['phone']   : '';
        $mobile = isset($_POST['mobile'])  ? $_POST['mobile']  : '';

        $contactMethods = isset($_POST['contact-methods']) ? $_POST['contact-methods'] : false;
        $interests = isset($_POST['interests']) ? $_POST['interests'] : false;

        $methods = [
            'email' => 'Email',
            'post' => 'Post',
            'phone' => 'Phone',
            'sms' => 'SMS',
        ];

        $customFields = [];


        foreach($contactMethods as $method) {
            if(!array_key_exists($method, $methods)) { continue; }

            $customFields[] = [
                'Key' => "Contact Methods",
                'Value' => $methods[$method],
            ];
        }

        $interests = array_map(function($interest) {
            return [
                'Key' => 'Interests',
                'Value' => $interest,
            ];
        }, $interests);

        $customFields = array_merge($customFields, $interests);

        $customFields[] = [
            'Key' => "Postal Address",
            'Value' => $addr,
        ];

        $customFields[] = [
            'Key' => "Phone Number",
            'Value' => $phone,
        ];

        $customFields[] = [
            'Key' => "Mobile Phone",
            'Value' => $mobile,
        ];

        foreach($interests as $method) {
            if(!array_key_exists($method, $methods)) { continue; }

            $customFields[] = [
                'Key' => "Contact Methods",
                'Value' => $methods[$method],
            ];
        }

        $object = [
            'Name' => $name,
            'EmailAddress' => $email,
            'CustomFields' => $customFields,
            'Resubscribe' => true
        ];

        $wrap = new \CS_REST_Subscribers($list, self::$api->auth);
        $result = $wrap->add($object);

        if(!$result->was_successful()) {
            self::error($result);
        }

        $result->Status = 'success';
        $result->Code = $result;

        self::showResult([
            'Status' => 'success',
            'Code' => $result->http_status_code,
            'Message' => self::$api->successMessage,
        ]);
    }
}



add_action('wp_ajax_'.CampaignMonitor::$ajaxAction, __NAMESPACE__.'\\CampaignMonitor::subscribe');
add_action('wp_ajax_nopriv_'.CampaignMonitor::$ajaxAction, __NAMESPACE__.'\\CampaignMonitor::subscribe');