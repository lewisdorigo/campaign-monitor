<?php
/**
 * Plugin Name:       Campaign Monitor
 * Plugin URI:        https://bitbucket.org/lewisdorigo/campaign-monitor
 * Description:       Campaign Monitor API for Wordpress
 * Version:           2.0.2
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */

namespace Dorigo\CampaignMonitor;

if(!defined('ABSPATH')) exit; // Exit if accessed directly

define("DRGO_CAMPAIGN_MONITOR", __FILE__);

if(is_dir(__DIR__.'/vendor')) {
    require_once(__DIR__.'/vendor/autoload.php');
}


require_once(__DIR__.'/settings.php');
require_once(__DIR__.'/campaign-monitor.class.php');


function enqueue_scripts() {
    wp_enqueue_script('dorigo-campaign-monitor',plugins_url('campaign-monitor.js', __FILE__), ['jquery'], false, true);
}

add_action('wp_enqueue_scripts', __NAMESPACE__.'\\enqueue_scripts');